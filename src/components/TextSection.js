import React from 'react';

export const TextSection = (props)=>{
	return(
			<ul>
				{Object.keys(props.data).filter(obj=>obj!=="Poster").map(info=>{
					return renderList(props.data[info]);
				})}
			</ul>
		)
}

const renderList = (data) => {
	return(
		<li>
			{data}
		</li>
	)
};