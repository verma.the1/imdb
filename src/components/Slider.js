import React from 'react'


export class Slider extends React.Component {

	constructor(props){
		super(props);
		this.state={
			value :1
		}
		this.onChangeSlider = this.onChangeSlider.bind(this);

	}


	onChangeSlider = (e) => {
		this.setState({
			value:e.target.value
		})
		this.props.onRating(e.target.value);
	}

	render(){
		return(
			<input type="range" min={this.props.min} max={this.props.max} value={this.state.value} onChange={this.onChangeSlider}/>
			);
	}

}

Slider.defaultProps = {
	min:1,
	max:10
}


