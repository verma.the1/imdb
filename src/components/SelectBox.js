import React from 'react'


export class SelectBox extends React.Component {

	constructor(props){
		super(props);
		this.state={
			value :"Very"
		}
		this.onChangereview = this.onChangereview.bind(this);

	}
	onChangereview = (e) => {
		this.setState({
			value:e.target.value
		})
		this.props.onChangereview(e.target.value);
	}

	render(){
		let sel = this.state.value;
		return(
				<select name="recommend" onChange={this.onChangereview} >
				    <option value="Less" selected ={sel === "Less"}>Less</option>
				    <option value="Very" selected = {sel === "Very"} >Very</option>
				    <option value="Extremely Likely" selected = {sel === "Extremely Likely"}>Extremely Likely</option>
				  </select>
			);
	}

}

