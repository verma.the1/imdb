import React, { Component } from 'react';
import { getList } from '../utils';
import { ImageComponent } from '../components/ImageComponent';
import { TextSection } from '../components/TextSection';
import { Link } from 'react-router-dom';


export class Explore extends Component {

	constructor(props){
		super(props);
		this.state = {
			data: ""
		}
		this.cardList = this.cardList.bind(this);
	}
	componentDidMount(){
		getList().then(res=>this.setState({data:res.data.Search}));

	}


	cardList(props){
		return(
				<Link to={"/description/"+props.imdbID}>
				<div className="card-section">
					<ImageComponent url ={props.Poster} />
					<TextSection data={props}/>
				</div>
				</Link>
			 );
		
	}
	render(){
		if(this.state.data){
			return(
				<div className="explore-section">
				{
					 this.state.data.map(data=>{
						return this.cardList(data)
					})	
				}

				</div>
			)
		}
		return(
			<div>Loading...</div>
			
		);
	}
}