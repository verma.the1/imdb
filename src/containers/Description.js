import React from 'react'
import { getDescriptionById } from '../utils';
import { Slider } from '../components/Slider';
import { SelectBox } from '../components/SelectBox';
import { ImageComponent } from '../components/ImageComponent';
//import { TextSection } from '../components/TextSection';


export class Description extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			ratingNumber : '',
			recommendToFriend :'' ,
			data: ""
		}
		this.ratingInput = this.ratingInput.bind(this);
	}

	componentDidMount(){
		const id = this.props.match.params.imdbID;
		getDescriptionById(id).then(res=>this.setState({
			data:res.data
		}));
	}

	ratingInput(ratingNumber){
		this.setState({
			ratingNumber
		})
	}

	recommendInput(recommendToFriend){
		
		this.setState({
			recommendToFriend
		})
	}

	renderUi = ()=>{
		return(
			<div className="desc-section">
				<div>
					<ImageComponent url ={this.state.data.Poster} />
				</div>
				<div className="rate-section">
					<div className="rate-sider">Rate this movie :<Slider onRating={(value)=>this.ratingInput(value)}/>  </div>
					<div className="recommend">Likely To recommend a Friend<SelectBox onChangereview={(value)=>this.recommendInput(value)}/></div>
          			<div><button onClick={this.handleSubmit} className="submit">Submit Review</button></div>
          		</div>
        		
			</div>

		);
	}

	render(){
		if(this.state.data){
			return(
			<div className="desc-section">
				<div>
					<ImageComponent url ={this.state.data.Poster} />
				</div>
				<div className="rate-section">
					<div className="rate-sider">Rate this movie :<Slider onRating={(value)=>this.ratingInput(value)}/>  </div>
					<div className="recommend">Likely To recommend a Friend<SelectBox onChangereview={(value)=>this.recommendInput(value)}/></div>
          			<div><button onClick={this.handleSubmit} className="submit">Submit Review</button></div>
          		</div>
        		
			</div>
		);
		}else{
			return(
				<div>Loading...</div>
				);
		}
	}
}


