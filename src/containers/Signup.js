import React from 'react'
import { Link } from 'react-router-dom'
import { saveToLocalStorage } from '../utils'

export class Signup extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      confirmPassword: '',
      firstName: '',
      lastName: '',
      error: '',
      errorMessage: ''
    }
  }

  onChangeEmail = e => {
    this.setState({
      email: e.target.value
    })
  }

  onChangePassword = e => {
    this.setState({
      password: e.target.value
    })
  }

  onChangeConfirmPassword = e => {
    this.setState({
      confirmPassword: e.target.value
    })
  }

  onChangeFirstName = e => {
    this.setState({
      firstName: e.target.value
    })
  }

  onChangeLastName = e => {
    this.setState({
      lastName: e.target.value
    })
  }

  setError = (isError, errorMessage) => {
    if(isError) {
      this.setState({
        error: isError,
        errorMessage
      })
    }
  }

  validateEmail = () => {
    const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    this.setError(!emailRegex.test(this.state.email), 'invalid email')
  }

  validatePassword = () => {
    this.setError(!this.state.password, 'invalid password')
  }

  validateConfirmPassword = () => {
    this.setError(this.state.password !== this.state.confirmPassword, 'password doesn\'t match')
  }

  validateFirstName = () => {
    this.setError(!this.state.firstName, 'firstname cannot be empty')
  }

  validateLastName = () => {
    this.setError(!this.state.lastName, 'lastname cannot be empty')
  }

  validateAll = () => {
    this.validateEmail();
    this.validatePassword();
    this.validateConfirmPassword();
    this.validateFirstName();
    this.validateLastName();
  }

  handleLoginClick = () => {
    const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    this.state.error = false;
    this.validateAll();
    if(!this.state.error) {
      if(this.state.lastName!=="" && this.state.firstName!=="" && this.state.password!=="" && this.state.confirmPassword!=="" && this.state.email!=="" && emailRegex.test(this.state.email)){
        saveToLocalStorage(this.state)
        this.props.history.replace('/login')
      }
      
    }
  }

  render() {
    return(
      <div>
        <button><Link to="/login" className="login-button" >Login Here</Link></button>
        <div>
          email: <input type="text" onChange={this.onChangeEmail}/>
        </div>
        <div>
          password: <input type="text" onChange={this.onChangePassword}/>
        </div>
        <div>
          confirm password: <input type="text" onChange={this.onChangeConfirmPassword}/>
        </div>
        <div>
          firstName: <input type="text" onChange={this.onChangeFirstName}/>
        </div>
        <div>
          lastName: <input type="text" onChange={this.onChangeLastName}/>
        </div>
        <div>
          <button className="register-now" onClick={this.handleLoginClick}>Register Now! </button>
        </div>
        {this.state.error ? <div>{this.state.errorMessage}</div> : null}
      </div>
    )
  }
}