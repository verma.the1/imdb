import React from 'react'
import { Link } from 'react-router-dom'
import { getStateFromLocalStorage } from '../utils'

export class Login extends React.Component {
  constructor() {
    super()
    this.state = {
      email: '',
      password: '',
      error: '',
    }
  } 

  onChangeEmail = e => {
    this.setState({
      email: e.target.value
    })
  }

  onChangePassword = e => {
    this.setState({
      password: e.target.value
    })
  }

  checkLogin = () => {
    const data = getStateFromLocalStorage();
    if(data){
      const user = data.filter(item =>
      item.email === this.state.email && item.password === this.state.password)
      if(user) {
      return user[0]
      }

    }
    
    return null
  }

  handleLoginClik = () => {
    const user = this.checkLogin()
    if(user) {
      this.setState({
        error: ''
      })
      this.props.history.replace('/dashboard')
    } else {
      this.setState({
        error: 'invalid username or password'
      })
    }
  }

  render() {
    return(
      <div className="signup">
        <div ><Link to="/signup" className="signup-btn">Sign up here </Link></div>
        <div>
          email: <input type="text" onChange={this.onChangeEmail}/>
        </div>
        <div>
          password: <input type="text" onChange={this.onChangePassword}/>
        </div>
        <div>
          <button className="login-now" onClick={this.handleLoginClik}>Login Now!!</button>
        </div>
        {this.state.error ? <div>{this.state.error}</div> : null}
      </div>
    )
  }
}