import React, { Component } from 'react';
import './App.css';
import { Login } from './containers/Login';
import { Signup } from './containers/Signup';
import { Dashboard } from './containers/Dashboard';
import { Explore } from './containers/Explore';
import { Description } from './containers/Description';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <div className="App-header">
     <Router>
      <Switch>
        <Route exact path = "/" component = { Signup }/>
        <Route  path = "/signUp" component = { Signup }/>
        <Route path="/login" component={Login} />
        <Route  path = "/dashboard" component = { Dashboard }/>
        <Route  path = "/explore" component = { Explore }/>
        <Route path = "/description/:imdbID" component = { Description }/>
      </Switch> 
    </Router>
    </div>
    );
  }
}

export default App;
