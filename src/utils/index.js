import axios from 'axios';

const API_HOST = 'http://www.omdbapi.com';
const API_KEY = '9f0bd725';

export const saveToLocalStorage = (state) => {
  const data = window.localStorage.getItem('state') || []
  const newData = [state, ...data]
  window.localStorage.setItem('state', JSON.stringify(newData))
}

export const getStateFromLocalStorage = () => {
  const serializedState = window.localStorage.getItem('state')
  return JSON.parse(serializedState)
}

export const getList =(params) => {
	const page = (params && params.page ) || 1;
	const search = (params && params.search) || "one";
	let filter = "/?s="+search+"&page="+page;
	return basicRequest(filter);
}

export const getDescriptionById = (id) => {
	let filter = "/?i="+id;
	return basicRequest(filter);
}

const basicRequest = (params) => {
	return axios.get(API_HOST+params+"&apikey="+API_KEY);
}